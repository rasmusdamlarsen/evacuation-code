def up():
    # Move the arrow up
    print("up")

def down():
    # Move the arrow down
    print("down")
    
def left():
    # Move the arrow left
    print("left")

def right():
    # Move the arrow right
    print("right")

def u_right():
    # Move the arrow up and to the right
    print("up and to the right")

def u_left():
    # Move the arrow up and to the left
    print("up and to the left")

def d_right():
    # Move the arrow down and to the right
    print("down and to the right")

def d_left():
    # Move the arrow down and to the left
    print("down and to the left")
