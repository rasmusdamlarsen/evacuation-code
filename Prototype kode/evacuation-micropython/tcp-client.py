import socket
import network
import machine
import arrow

# Powershell command to send a request to the device
# echo "command" | ncat 192.168.137.100 80

right = machine.Pin(0, machine.Pin.OUT)
left = machine.Pin(4, machine.Pin.OUT)

# Initialize Ethernet interface
eth = network.LAN(mdc = machine.Pin(23), mdio = machine.Pin(18), power = machine.Pin(12), phy_type = network.PHY_LAN8720, phy_addr = 0)
eth.active(1)

# Set a static IP address
ip_address = '192.168.137.100'  # Change this to an appropriate IP for your network
subnet_mask = '255.255.255.0'  # Typically this value, but adjust if your network differs
gateway = '192.168.137.1'  # Typically your router's IP address
dns = '8.8.8.8'  # Google's DNS, but replace if you prefer a different one
eth.ifconfig((ip_address, subnet_mask, gateway, dns))

# Verify the connection
print('Static IP configuration successful')
print('IP Address:', eth.ifconfig()[0])

# Create a socket and listen for connections on the device IP address
addr = socket.getaddrinfo(ip_address, 80)[0][-1]

s = socket.socket()
s.bind(addr)
s.listen(1)

print('Listening on', addr)

while True:
    cl, addr = s.accept()
    print('Client connected from', addr)
    request = cl.recv(1024)
    request = str(request)
    print('Content = %s' % request)
    
    if 'up' in request:
        print('up')
        arrow.up()
        right.value(1)
        left.value(0)
        
    elif 'down' in request:
        print('down')
        arrow.down()
        right.value(1)
        left.value(0)

    elif 'left' in request:
        print('left')
        arrow.left()
        right.value(0)
        left.value(1)

    elif 'right' in request:
        print('right')
        arrow.right()
        right.value(1)
        left.value(0)

    elif 'u_right' in request:
        print('up and to the right')
        arrow.u_right()
        right.value(1)
        left.value(0)

    elif 'u_left' in request:
        print('up and to the left')
        arrow.u_left()
        right.value(0)
        left.value(1)

    elif 'd_right' in request:
        print('down and to the right')
        arrow.d_right()
        right.value(1)
        left.value(0)

    elif 'd_left' in request:
        print('down and to the left')
        arrow.d_left()
        right.value(0)
        left.value(1)

    else:
        print('Unknown command')
        right.value(0)
        left.value(0)
    
    cl.send('Response message\n')
    cl.close()
