from hub75 import Hub75
import utime

# Initialize the matrix with the correct pins
matrix = Hub75(25, 26, 27, 14, 12, 13, 15, 2, 4, 5, 32, 18, 19, 21)

def read_rgb_line(line):
    values = list(map(int, line.strip().split(',')))
    rgb_line = [(values[i], values[i+1], values[i+2]) for i in range(0, len(values), 3)]
    return rgb_line

def display_image(filename):
    with open(filename) as f:
        for y, line in enumerate(f):
            if y < matrix.height:
                rgb_line = read_rgb_line(line)
                for x in range(matrix.width):
                    matrix.set_pixel(x, y, rgb_line[x])
            else:
                break

    while True:
        matrix.display()
        utime.sleep(0.1)

# Ensure the filename matches the file you have uploaded
display_image('right_arrow.txt')
