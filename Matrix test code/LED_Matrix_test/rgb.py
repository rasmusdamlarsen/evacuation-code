# rgb.py - Simplified library for HUB75 LED Matrix

import time
from machine import Pin

class RGBMatrix:
    def __init__(self, pin_map, width=64, height=64, double_buffer=False):
        self.width = width
        self.height = height
        self.double_buffer = double_buffer
        
        self.pins = {}
        for key, pin in pin_map.items():
            self.pins[key] = Pin(pin, Pin.OUT)
        
        self.buffer = [[(0, 0, 0) for _ in range(width)] for _ in range(height)]
    
    def pixel(self, x, y, color):
        self.buffer[y][x] = color
    
    def show(self):
        for row in range(self.height // 2):
            self._set_row_address(row)
            for col in range(self.width):
                upper_pixel = self.buffer[row][col]
                lower_pixel = self.buffer[row + (self.height // 2)][col]
                self._set_pixel(upper_pixel, lower_pixel)
            self._latch()
    
    def _set_row_address(self, row):
        self.pins['a'].value(row & 0x01)
        self.pins['b'].value((row >> 1) & 0x01)
        self.pins['c'].value((row >> 2) & 0x01)
        self.pins['d'].value((row >> 3) & 0x01)
        self.pins['e'].value((row >> 4) & 0x01)
    
    def _set_pixel(self, upper_pixel, lower_pixel):
        self.pins['r1'].value(upper_pixel[0])
        self.pins['g1'].value(upper_pixel[1])
        self.pins['b1'].value(upper_pixel[2])
        self.pins['r2'].value(lower_pixel[0])
        self.pins['g2'].value(lower_pixel[1])
        self.pins['b2'].value(lower_pixel[2])
        self.pins['clk'].value(1)
        self.pins['clk'].value(0)
    
    def _latch(self):
        self.pins['oe'].value(1)
        self.pins['lat'].value(1)
        self.pins['lat'].value(0)
        self.pins['oe'].value(0)
