import machine
import utime

class Hub75:
    def __init__(self, r1, g1, b1, r2, g2, b2, a, b, c, d, e, clk, lat, oe):
        self.r1 = machine.Pin(r1, machine.Pin.OUT)
        self.g1 = machine.Pin(g1, machine.Pin.OUT)
        self.b1 = machine.Pin(b1, machine.Pin.OUT)
        self.r2 = machine.Pin(r2, machine.Pin.OUT)
        self.g2 = machine.Pin(g2, machine.Pin.OUT)
        self.b2 = machine.Pin(b2, machine.Pin.OUT)
        self.a = machine.Pin(a, machine.Pin.OUT)
        self.b = machine.Pin(b, machine.Pin.OUT)
        self.c = machine.Pin(c, machine.Pin.OUT)
        self.d = machine.Pin(d, machine.Pin.OUT)
        self.e = machine.Pin(e, machine.Pin.OUT)
        self.clk = machine.Pin(clk, machine.Pin.OUT)
        self.lat = machine.Pin(lat, machine.Pin.OUT)
        self.oe = machine.Pin(oe, machine.Pin.OUT)

        self.width = 64
        self.height = 64

        self.buffer = [[(0, 0, 0) for _ in range(self.width)] for _ in range(self.height)]

    def set_pixel(self, x, y, color):
        self.buffer[y][x] = color

    def display(self):
        for row in range(0, self.height, 2):
            self.oe.on()  # Disable output while changing rows
            self._set_row_address(row // 2)
            for col in range(self.width):
                self._set_row(row, col)
            self.lat.on()
            utime.sleep_us(10)  # Short delay to latch the data
            self.lat.off()
            self.oe.off()  # Enable output
            utime.sleep_ms(1)  # Short delay to stabilize the display

    def _set_row(self, row, col):
        r1, g1, b1 = self.buffer[row][col]
        r2, g2, b2 = self.buffer[row + 1][col]

        self.r1.value(r1)
        self.g1.value(g1)
        self.b1.value(b1)
        self.r2.value(r2)
        self.g2.value(g2)
        self.b2.value(b2)

        self.clk.on()
        self.clk.off()

    def _set_row_address(self, row):
        self.a.value((row >> 0) & 1)
        self.b.value((row >> 1) & 1)
        self.c.value((row >> 2) & 1)
        self.d.value((row >> 3) & 1)
        self.e.value((row >> 4) & 1)
