from hub75 import Hub75
import utime

# Initialize the matrix with the correct pins
matrix = Hub75(25, 26, 27, 14, 12, 13, 15, 2, 4, 5, 32, 18, 19, 21)

# Create a simple 8x8 checkerboard pattern
def create_test_image():
    for y in range(8):
        for x in range(8):
            if (x + y) % 2 == 0:
                matrix.set_pixel(x, y, (1, 0, 0))  # Red
            else:
                matrix.set_pixel(x, y, (0, 1, 0))  # Green

create_test_image()

while True:
    matrix.display()
    utime.sleep(0.1)
